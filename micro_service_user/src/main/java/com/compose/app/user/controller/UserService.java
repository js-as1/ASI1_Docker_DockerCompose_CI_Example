package com.compose.app.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compose.app.user.model.User;
import com.compose.app.user.model.UserDto;

@Service
public class UserService {
	@Autowired
	UserRepository uRepository;
	public void addUser(UserDto uDto) {
		User u=new User();
		u.setParameters(uDto);
		User createdUser=uRepository.save(u);
		System.out.println(createdUser);
	}
	
	public User getHero(int id) {
		Optional<User> uOpt =uRepository.findById(id);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
	}

	public List<User> getAllUser() {
		List<User> userList = new ArrayList<>();
		uRepository.findAll().forEach(userList::add);
		return userList;
	}

}
