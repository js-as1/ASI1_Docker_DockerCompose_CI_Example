package com.compose.app.user.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.compose.app.user.controller.UserService;
import com.compose.app.user.model.User;
import com.compose.app.user.model.UserDto;

@RestController
public class UserRestCrt {
	@Autowired
	UserService uService;
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public void addUser(@RequestBody UserDto uDto) {
		uService.addUser(uDto);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/user/{id}")
	public User getUser(@PathVariable String id) {
		User h=uService.getHero(Integer.valueOf(id));
		return h;
	}
	
	
	@RequestMapping(method=RequestMethod.GET,value="/user")
	public List<User> getAllUser() {
		return uService.getAllUser();
	}

}
